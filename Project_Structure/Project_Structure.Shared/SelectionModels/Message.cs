﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.SelectionModels
{
    public class Message
    {
        public string Text { get; set; }
        public DateTime Create_At { get; set; }
    }
}
