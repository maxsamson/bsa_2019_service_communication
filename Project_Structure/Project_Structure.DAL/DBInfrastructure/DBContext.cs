﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.DBInfrastructure
{
    public class DBContext
    {
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }

        public DBContext()
        {
            TasksInit();
            UsersInit();
            TeamsInit();
            ProjectsInit();
        }

        private void TasksInit()
        {
            Tasks = new List<Task>();

            Tasks.Add(
                new Task
                {
                    Id = 1,
                    Name = "Eaque corporis illum ut.",
                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                    Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                    Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                    State = State.Started,
                    Performer = new User(),
                    Project = new Project()
                });
            Tasks.Add(
                new Task
                {
                    Id = 2,
                    Name = "Debitis quis ad quas voluptatem voluptatem.",
                    Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.\nNatus officia quis.\nDeleniti sit earum eveniet.\nCumque magni quis eaque atque natus expedita ratione illum.",
                    Created_At = new DateTime(2019, 6, 18, 19, 31, 15),
                    Finished_At = new DateTime(2019, 8, 9, 3, 39, 45),
                    State = State.Created,
                    Performer = new User(),
                    Project = new Project()
                });
            Tasks.Add(
                new Task
                {
                    Id = 3,
                    Name = "Officia quas dolorem quod veniam vitae odit voluptates.",
                    Description = "Ut et consequatur quo.\nIpsa rerum qui.\nDelectus sunt voluptatibus facilis facere minima repellat deserunt temporibus.",
                    Created_At = new DateTime(2019, 6, 19, 0, 56, 43),
                    Finished_At = new DateTime(2020, 3, 31, 17, 26, 25),
                    State = State.Started,
                    Performer = new User(),
                    Project = new Project()
                });
            Tasks.Add(
                new Task
                {
                    Id = 4,
                    Name = "Aut occaecati officiis dolor culpa porro.",
                    Description = "Voluptatem ipsum deleniti quisquam est aut accusamus porro veritatis quia.\nOccaecati et excepturi eius temporibus quia distinctio aspernatur.\nSoluta qui consequuntur est possimus itaque dolores ut.\nFacilis occaecati cupiditate omnis cumque.",
                    Created_At = new DateTime(2019, 6, 19, 1, 1, 47),
                    Finished_At = new DateTime(2019, 7, 7, 16, 54, 47),
                    State = State.Started,
                    Performer = new User(),
                    Project = new Project()
                });
            Tasks.Add(
                new Task
                {
                    Id = 5,
                    Name = "Eos adipisci dignissimos minus non est est minus.",
                    Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                    Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                    Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                    State = State.Finished,
                    Performer = new User(),
                    Project = new Project()
                });
        }

        private void UsersInit()
        {
            Users = new List<User>();

            Users.Add(
                new User
                {
                    Id = 1,
                    First_Name = "Gayle",
                    Last_Name = "Swift",
                    Email = "Gayle_Swift98@yahoo.com",
                    Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                    Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                    Projects = new List<Project>(),
                    Tasks = new List<Task>(),
                    Team = new Team()
                });
            Tasks[0].Performer = Users[0];
            Tasks[1].Performer = Users[0];
            Users[0].Tasks = new List<Task>(Tasks.FindAll(t => t.Performer.Id == Users[0].Id));

            Users.Add(
                new User
                {
                    Id = 2,
                    First_Name = "Tad",
                    Last_Name = "Powlowski",
                    Email = "Tad_Powlowski27@hotmail.com",
                    Birthday = new DateTime(2017, 10, 11, 12, 23, 43),
                    Registered_At = new DateTime(2019, 6, 11, 2, 3, 33),
                    Projects = new List<Project>(),
                    Tasks = new List<Task>(),
                    Team = new Team()
                });
            Tasks[2].Performer = Users[1];
            Users[1].Tasks = new List<Task>(Tasks.FindAll(t => t.Performer.Id == Users[1].Id));

            Users.Add(
                new User
                {
                    Id = 3,
                    First_Name = "Letha",
                    Last_Name = "Strosin",
                    Email = "Letha.Strosin58@gmail.com",
                    Birthday = new DateTime(2002, 5, 11, 15, 44, 15),
                    Registered_At = new DateTime(2019, 6, 17, 2, 56, 24),
                    Projects = new List<Project>(),
                    Tasks = new List<Task>(),
                    Team = new Team()
                });
            Tasks[3].Performer = Users[2];
            Tasks[4].Performer = Users[2];
            Users[2].Tasks = new List<Task>(Tasks.FindAll(t => t.Performer.Id == Users[2].Id));
        }

        private void TeamsInit()
        {
            Teams = new List<Team>();

            Teams.Add(
                new Team
                {
                    Id = 1,
                    Name = "labore",
                    Created_At = new DateTime(2019,6,19,1,0,24),
                    Users = new List<User>() { Users[0], Users[1]},
                    Projects = new List<Project>()
                });
            Users[0].Team = Teams[0];
            Users[1].Team = Teams[0];

            Teams.Add(
                new Team
                {
                    Id = 2,
                    Name = "sunt",
                    Created_At = new DateTime(2019, 6, 19, 6, 38, 3),
                    Users = new List<User>() { Users[2] },
                    Projects = new List<Project>()
                });
            Users[2].Team = Teams[1];
        }

        private void ProjectsInit()
        {
            Projects = new List<Project>();

            Projects.Add(
                new Project
                {
                    Id = 1,
                    Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                    Created_At = new DateTime(2019,6,18,13,46,19),
                    Deadline = new DateTime(2020,1,13,6,24,33),
                    Author = Users[0],
                    Team = Teams[1],
                    Tasks = new List<Task>() { Tasks[0], Tasks[4]}
                });
            Tasks[0].Project = Projects[0];
            Tasks[4].Project = Projects[0];
            Users[0].Projects.Add(Projects[0]);
            Teams[1].Projects.Add(Projects[0]);

            Projects.Add(
                new Project
                {
                    Id = 2,
                    Name = "Atque quia et optio aut.",
                    Description = "Non non aperiam aspernatur et est mollitia enim quia nihil.\nLibero adipisci quisquam earum fugiat et perferendis explicabo.",
                    Created_At = new DateTime(2019, 6, 18, 20, 12, 38),
                    Deadline = new DateTime(2020, 5, 9, 12, 34, 30),
                    Author = Users[1],
                    Team = Teams[0],
                    Tasks = new List<Task>() { Tasks[1], Tasks[3] }
                });
            Tasks[1].Project = Projects[1];
            Tasks[3].Project = Projects[1];
            Users[2].Projects.Add(Projects[1]);
            Teams[0].Projects.Add(Projects[1]);

            Projects.Add(
                new Project
                {
                    Id = 3,
                    Name = "Harum rerum ipsa quidem et.",
                    Description = "Cupiditate veritatis ad rerum veritatis molestiae.\nDolorem soluta adipisci fuga iste aut est eveniet et.\nMolestiae ut nisi officia nobis consequatur neque sint.\nAut in non officia illo minus.\nVoluptas ut est qui dolore distinctio sint delectus.",
                    Created_At = new DateTime(2019, 6, 18, 23, 17, 38),
                    Deadline = new DateTime(2019, 9, 14, 16, 45, 32),
                    Author = Users[1],
                    Team = Teams[1],
                    Tasks = new List<Task>() { Tasks[2] }
                });
            Tasks[2].Project = Projects[2];
            Users[1].Projects.Add(Projects[2]);
            Teams[1].Projects.Add(Projects[2]);
        }

        public List<TEntity> SetOf<TEntity>() where TEntity : Entity
        {
            if (Projects is List<TEntity>)
                return Projects as List<TEntity>;
            else if (Tasks is IEnumerable<TEntity>)
                return Tasks as List<TEntity>;
            else if (Teams is IEnumerable<TEntity>)
                return Teams as List<TEntity>;
            else return Users as List<TEntity>;
        }
    }
}
