﻿using DAL.DBInfrastructure;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly DBContext DbContext;

        public BaseRepository(DBContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual int Create(TEntity entity)
        {
            entity.Id = DbContext.SetOf<TEntity>().Count + 1;
            DbContext.SetOf<TEntity>().Add(entity);
            return entity.Id;
        }

        public virtual void Delete(TEntity entity)
        {
            DbContext.SetOf<TEntity>().Remove(entity);
        }

        public virtual void DeleteById(int id)
        {
            DbContext.SetOf<TEntity>().Remove(DbContext.SetOf<TEntity>().Find(entity => entity.Id == id));
        }

        public virtual List<TEntity> GetAll()
        {
            return DbContext.SetOf<TEntity>();
        }

        public virtual TEntity GetById(int id)
        {
            return DbContext.SetOf<TEntity>().Find(entity => entity.Id == id);
        }

        public virtual void Update(int id, TEntity entity)
        {
            var temp = DbContext.SetOf<TEntity>().Find(x => x.Id == id);
            temp = entity;
        }
    }
}
