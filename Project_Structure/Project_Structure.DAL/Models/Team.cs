﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }
    }
}
