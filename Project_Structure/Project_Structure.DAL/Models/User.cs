﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class User : Entity
    {
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_At { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Project> Projects { get; set; }
        public Team Team { get; set; }
    }
}
