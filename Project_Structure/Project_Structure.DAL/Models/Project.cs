﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public List<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
}
