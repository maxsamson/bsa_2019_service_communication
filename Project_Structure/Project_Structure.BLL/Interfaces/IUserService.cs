﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Interfaces
{
    public interface IUserService
    {
        List<UserDTO> GetAll();

        UserDTO GetById(int id);

        int Create(UserDTO user);

        void Update(int id, UserDTO user);

        void Delete(UserDTO user);

        void DeleteById(int id);
    }
}
