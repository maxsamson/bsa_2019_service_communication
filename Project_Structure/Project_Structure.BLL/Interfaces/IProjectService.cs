﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectService
    {
        List<ProjectDTO> GetAll();

        ProjectDTO GetById(int id);

        int Create(ProjectDTO project);

        void Update(int id, ProjectDTO project);

        void Delete(ProjectDTO project);

        void DeleteById(int id);
    }
}
