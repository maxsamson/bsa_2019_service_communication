﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITaskService
    {
        List<TaskDTO> GetAll();

        TaskDTO GetById(int id);

        int Create(TaskDTO task);

        void Update(int id, TaskDTO task);

        void Delete(TaskDTO task);

        void DeleteById(int id);
    }
}
