﻿using DAL.Models;
using DAL.Repositories;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.SelectionModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Project_Structure.BLL.Services
{
    public class SelectionService : ISelectionService
    {
        private readonly ProjectRepository projectRepo;
        private readonly TaskRepository taskRepo;
        private readonly TeamRepository teamRepo;
        private readonly UserRepository userRepo;
        private readonly IMapper mapper;

        public SelectionService(ProjectRepository projectRepo, TaskRepository taskRepo, TeamRepository teamRepo, UserRepository userRepo, IMapper mapper)
        {
            this.projectRepo = projectRepo;
            this.taskRepo = taskRepo;
            this.teamRepo = teamRepo;
            this.userRepo = userRepo;
            this.mapper = mapper;
        }

        public List<ProjectDTO> CreateHierarhy()
        {
            try
            {
                return mapper.MapProjects(projectRepo.GetAll());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public List<TasksCount> TasksCount(int user_Id)
        {
            try
            {
                return projectRepo.GetAll()
                    .Where(p => p.Author.Id == user_Id)
                    .Select(p => new TasksCount
                    {
                        Project = mapper.MapProject(p),
                        Count = p.Tasks.Count()
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public List<TaskDTO> TasksWithNameLess45Symbols(int user_Id)
        {
            try
            {
                return mapper.MapTasks(userRepo.GetAll()
                    .Where(item => item.Id == user_Id)
                    .SelectMany(item => item.Tasks.Where(t => t.Name.Length < 45)).ToList());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<TasksFinished2019> TasksFinished2019(int user_Id)
        {
            try
            {
                return userRepo.GetAll()
                    .Where(item => item.Id == user_Id)
                    .SelectMany(item => item.Tasks.Where(t => t.State == State.Finished && t.Finished_At.Year == 2019))
                    .Select(t => new TasksFinished2019()
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<Teams12yearsGroup> Teams12yearsGroup()
        {
            try
            {
                return teamRepo.GetAll()
                    .Where(t => t.Users.Count > 0 && t.Users.All(u => DateTime.Now.Year - u.Birthday.Year > 12))
                    .Select(item => new Teams12yearsGroup
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Users = mapper.MapUsers(item.Users)
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<UsersACSTasksDSC> UsersACSTasksDSC()
        {
            try
            {
                return userRepo.GetAll()
                    .OrderBy(u => u.First_Name)
                    .Select(item => new UsersACSTasksDSC
                    {
                        User = mapper.MapUser(item),
                        Tasks = mapper.MapTasks(item.Tasks.OrderByDescending(t => t.Name.Length).ToList())
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<UserStruct> UserStruct(int user_Id)
        {
            try
            {
                return userRepo.GetAll()
                    .Where(item => item.Id == user_Id)
                    .Select(item => new UserStruct
                    {
                        User = mapper.MapUser(item),
                        LastProject = mapper.MapProject(item.Projects.OrderByDescending(p => p.Created_At).First()),
                        LastProjectTasksCount = item.Projects.OrderByDescending(p => p.Created_At).First().Tasks.Count(),
                        NotFinishedCanceledTasks = mapper.MapTasks(item.Tasks.Where(t => t.State != State.Finished).ToList()),
                        MaxDurationTask = mapper.MapTask(item.Tasks.OrderBy(t => t.Created_At).OrderByDescending(t => t.Finished_At).First())
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<ProjectStruct> ProjectStruct(int proj_Id)
        {
            try
            {
                return projectRepo.GetAll().Where(item => item.Id == proj_Id).Select(item => new ProjectStruct
                {
                        Project = mapper.MapProject(item),
                        LongestTask = mapper.MapTask(item.Tasks.OrderByDescending(t => t.Description).FirstOrDefault()),
                        ShortestTask = mapper.MapTask(item.Tasks.OrderBy(t => t.Name).FirstOrDefault()),
                        UsersCount = item.Description.Length > 25 || item.Tasks.Count < 3 ? item.Team.Users.Count() : 0
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<Message> GetMessages(string path)
        {
            try
            {
                List<Message> result = new List<Message>();

                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var list = line.Split("\t");
                        result.Add(new Message()
                        {
                            Create_At = Convert.ToDateTime(list[0]),
                            Text = list[1]
                        });
                    }
                }

                return result;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
