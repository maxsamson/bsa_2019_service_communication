﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly ProjectRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<ProjectDTO> validator;

        public ProjectService(ProjectRepository projectRepository, IMapper mapper, AbstractValidator<ProjectDTO> validator)
        {
            this.repository = projectRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        public List<ProjectDTO> GetAll()
        {
            var result = new List<ProjectDTO>();
            foreach (var item in repository.GetAll())
            {
                result.Add(mapper.MapProject(item));
            }
            return result;
        }

        public ProjectDTO GetById(int id)
        {
            return mapper.MapProject(repository.GetById(id));
        }

        public int Create(ProjectDTO project)
        {
            var validationResult = validator.Validate(project);
            if (validationResult.IsValid)
                return repository.Create(mapper.MapProject(project));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public void Update(int id, ProjectDTO project)
        {
            var validationResult = validator.Validate(project);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                repository.Update(id, mapper.MapProject(project));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(ProjectDTO project)
        {
            repository.Delete(mapper.MapProject(project));
        }

        public void DeleteById(int id)
        {
            repository.DeleteById(id);
        }
    }
}
