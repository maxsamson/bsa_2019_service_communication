﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Teams")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        readonly ITeamService service;
        readonly QueueService queueService;

        public TeamsController(ITeamService teamService, QueueService queueService)
        {
            service = teamService;
            this.queueService = queueService;
        }

        // GET: api/Teams
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(service.GetAll());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Teams/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                queueService.PostValueWorker($"Loading team with id={id} was triggered");

                return Ok(service.GetById(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Teams
        [HttpPost]
        public IActionResult Post([FromBody]TeamDTO team)
        {
            try
            {
                queueService.PostValueWorker($"Team creation was triggered");

                service.Create(team);
                return Ok();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Teams/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] TeamDTO team)
        {
            try
            {
                queueService.PostValueWorker($"Team modification with id={id} was triggered");

                service.Update(id, team);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Teams/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                queueService.PostValueWorker($"Deleting team with id={id} was triggered");

                service.DeleteById(id);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Teams
        [HttpDelete]
        public IActionResult Delete([FromBody] TeamDTO team)
        {
            try
            {
                queueService.PostValueWorker($"Deleting team was triggered");

                service.Delete(team);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}