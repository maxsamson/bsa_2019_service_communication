﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Tasks")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        readonly ITaskService service;
        readonly QueueService queueService;

        public TasksController(ITaskService taskService, QueueService queueService)
        {
            service = taskService;
            this.queueService = queueService;
        }

        // GET: api/Tasks
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(service.GetAll());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                queueService.PostValueWorker($"Loading task with id={id} was triggered");

                return Ok(service.GetById(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Tasks
        [HttpPost]
        public IActionResult Post([FromBody]TaskDTO task)
        {
            try
            {
                queueService.PostValueWorker($"Task creation was triggered");

                service.Create(task);
                return Ok();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Tasks/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] TaskDTO task)
        {
            try
            {
                queueService.PostValueWorker($"Task modification with id={id} was triggered");

                service.Update(id, task);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Tasks/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                queueService.PostValueWorker($"Deleting task with id={id} was triggered");

                service.DeleteById(id);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Tasks
        [HttpDelete]
        public IActionResult Delete([FromBody] TaskDTO task)
        {
            try
            {
                queueService.PostValueWorker($"Deleting task was triggered");

                service.Delete(task);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}