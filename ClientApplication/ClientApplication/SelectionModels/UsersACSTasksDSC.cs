﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace ClientApplication.SelectionModels
{
    public class UsersACSTasksDSC
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
