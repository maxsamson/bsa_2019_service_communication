﻿using ClientApplication.Models;

namespace ClientApplication.SelectionModels
{
    public class TasksCount
    {
        public Project Project { get; set; }
        public int Count { get; set; }
    }
}
