﻿using ClientApplication.Models;

namespace ClientApplication.SelectionModels
{
    public class ProjectStruct
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int UsersCount { get; set; }
    }
}
