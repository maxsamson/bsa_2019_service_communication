﻿using ClientApplication.Models;
using ClientApplication.SelectionModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientApplication.ServiceSettings
{
    public class Service
    {
        public List<Project> projects;
        public List<Task> tasks;
        public List<Team> teams;
        public List<User> users;
        private readonly string uri = "Selections/";

        public Service()
        {
            projects = ProjectsInit();
            tasks = TasksInit();
            teams = TeamsInit();
            users = UsersInit();
        }

        private List<Task> TasksInit()
        {
            return Request.GetDataList<Task>("Tasks");
        }

        private List<User> UsersInit()
        {
            return Request.GetDataList<User>("Users");
        }

        private List<Team> TeamsInit()
        {
            return Request.GetDataList<Team>("Teams");
        }

        private List<Project> ProjectsInit()
        {
            return Request.GetDataList<Project>("Projects");
        }

        public List<Project> CreateHierarhy()
        {
            try
            {
                return Request.GetDataList<Project>(uri + "CreateHierarhy");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public List<TasksCount> TasksCount(int user_Id)
        {
            try
            {
                return Request.GetDataList<TasksCount>(uri + $"TasksCount/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public List<Task> TasksWithNameLess45Symbols(int user_Id)
        {
            try
            {
                return Request.GetDataList<Task>(uri + $"TasksWithNameLess45Symbols/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<TasksFinished2019> TasksFinished2019(int user_Id)
        {
            try
            {
                return Request.GetDataList<TasksFinished2019>(uri + $"TasksFinished2019/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<Teams12yearsGroup> Teams12yearsGroup()
        {
            try
            {
                return Request.GetDataList<Teams12yearsGroup>(uri + "Teams12yearsGroup");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<UsersACSTasksDSC> UsersACSTasksDSC()
        {
            try
            {
                return Request.GetDataList<UsersACSTasksDSC>(uri + "UsersACSTasksDSC");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<UserStruct> UserStruct(int user_Id)
        {
            try
            {
                return Request.GetDataList<UserStruct>(uri + $"UserStruct/{user_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<ProjectStruct> ProjectStruct(int proj_Id)
        {
            try
            {
                return Request.GetDataList<ProjectStruct>(uri + $"ProjectStruct/{proj_Id}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public List<Message> GetMessages()
        {
            try
            {
                return Request.GetDataList<Message>(uri + "GetMessages");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }
    }
}
