﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ClientApplication.ServiceSettings
{
    public static class Request
    {
        private static JsonSerializerSettings settings = new JsonSerializerSettings();
        private static readonly Uri _url = new Uri("http://localhost:65430/api/");
        //https://localhost:44321/api/

        static Request()
        {
            settings.DateFormatString = "YYYY-MM-DDTHH:mm:ss.FFFZ";
        }

        public static List<T> GetDataList<T>(string uri)
        {
            HttpClientHandler clientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
            };

            using (HttpClient client = new HttpClient(clientHandler))
            {
                var result = client.GetStringAsync(_url + uri).Result;

                return JsonConvert.DeserializeObject<List<T>>(result, settings);
            }
        }
    }
}
