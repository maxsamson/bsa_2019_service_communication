﻿using ClientApplication.MenuPanel;
using ClientApplication.ServiceSettings;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ClientApplication
{
    class Program
    {
        private static HubConnection connection;
        public static List<Menu> ListOfMenus = new List<Menu>();
        public static Service service;

        static void Main(string[] args)
        {
            ConnectionToHub();

            ListOfMenus.Add(new Menu(StartService, Exit));
            ListOfMenus.Add(new Menu(CreateHierarhy, TasksCount, TasksWithNameLess45Symbols, TasksFinished2019, Teams12yearsGroup, UsersACSTasksDSC, UserStruct, ProjectStruct, GetMessages, Exit));
            ListOfMenus.Add(new Menu(TurnBack));

            ListOfMenus[0].Show(false);
        }

        public static void ConnectionToHub()
        {
            connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:65430/hub")
                .Build();
            connection.StartAsync().Wait();
            connection.On("PostResult", new Action<string>(PostResult));
        }

        public static void PostResult(string value)
        {
            Console.WriteLine($"\nMessage from Worker: {value}\n");
        }

        private static void Exit()
        {
            Environment.Exit(0);
        }

        private static void StartService()
        {
            Console.Clear();
            service = new Service();
            ListOfMenus[1].Show(false);
        }

        private static void TurnBack()
        {
            Console.Clear();
            ListOfMenus[1].Show(false);
        }

        private static void CreateHierarhy()
        {
            Console.Clear();
            try
            {
                var result = service.CreateHierarhy();
                foreach (var item in result)
                {
                    Console.WriteLine($"Project id: {item.Id};");
                    Console.WriteLine($"project name: \"{item.Name}\";");
                    Console.WriteLine($"author id: {item.Author_Id};");
                    Console.WriteLine($"team id: {item.Team_Id};");
                    Console.WriteLine("Tasks:");
                    int i = 1;
                    foreach (var task in item.Tasks)
                    {
                        Console.WriteLine($"{i++}. Task id: {task.Id}; task name: \"{task.Name}\".");
                    }
                    Console.WriteLine("----------------------------------------------------------------------");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void TasksCount()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.TasksCount(id);
                if (result.Count == 0)
                    Console.WriteLine("The user with this id has no projects.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Project id: {item.Project.Id};");
                        Console.WriteLine($"Project name: \"{item.Project.Name}\";");
                        Console.WriteLine($"Tasks count: {item.Count}.");
                        Console.WriteLine("------------------------------------------");
                    }
            }
            catch (Exception)
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void TasksWithNameLess45Symbols()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.TasksWithNameLess45Symbols(id);
                if (result.Count == 0)
                    Console.WriteLine("There are no tasks that satisfy this condition.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Task id: {item.Id};");
                        Console.WriteLine($"Task name: \"{item.Name}\".");
                        Console.WriteLine("-------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void TasksFinished2019()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.TasksFinished2019(id);
                if (result.Count == 0)
                    Console.WriteLine("The user has no such tasks.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Task id: {item.Id};");
                        Console.WriteLine($"Task name: \"{item.Name}\".");
                        Console.WriteLine("---------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void Teams12yearsGroup()
        {
            try
            {
                Console.Clear();
                var result = service.Teams12yearsGroup();
                if (result.Count == 0)
                    Console.WriteLine("There are no teams that satisfy this condition.");
                else
                    foreach (var item in result)
                    {
                        Console.WriteLine($"Team id: {item.Id};");
                        Console.WriteLine($"Team name: \"{item.Name}\";");
                        Console.WriteLine("Users:");
                        int i = 1;
                        foreach (var user in item.Users)
                        {
                            Console.WriteLine($"{i++}. User id: {user.Id}; User name: \"{user.First_Name} {user.Last_Name}\".");
                        }
                        Console.WriteLine("----------------------------------------");
                    }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void UsersACSTasksDSC()
        {
            try
            {
                Console.Clear();
                var result = service.UsersACSTasksDSC();
                foreach (var item in result)
                {
                    Console.WriteLine($"User id: {item.User.Id};");
                    Console.WriteLine($"User name: \"{item.User.First_Name} {item.User.Last_Name}\";");
                    Console.WriteLine("Tasks:");
                    int i = 1;
                    foreach (var task in item.Tasks)
                    {
                        Console.WriteLine($"{i++}. Task id: {task.Id}; Task name: \"{task.Name}\".");
                    }
                    Console.WriteLine("--------------------------------------------");
                }
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void UserStruct()
        {
            Console.Clear();
            Console.WriteLine("Write user's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.UserStruct(id).First();

                Console.WriteLine($"User id: {result.User.Id};");
                Console.WriteLine($"User name: \"{result.User.First_Name} {result.User.Last_Name}\";");
                Console.WriteLine($"Last project id: {result.LastProject.Id};");
                Console.WriteLine($"Last project name: \"{result.LastProject.Name}\";");
                Console.WriteLine($"Last project's tasks count: {result.LastProjectTasksCount}");
                Console.WriteLine($"Not finished or canceled tasks:");
                int i = 1;
                foreach (var task in result.NotFinishedCanceledTasks)
                {
                    Console.WriteLine($"{i++}. Task id: {task.Id}; task name: \"{task.Name}\".");
                }
                Console.WriteLine($"Longest task id: {result.MaxDurationTask.Id};");
                Console.WriteLine($"Longest task name: \"{result.MaxDurationTask.Name}\".");
                Console.WriteLine("--------------------------------------");
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void ProjectStruct()
        {
            Console.Clear();
            Console.WriteLine("Write project's id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                var result = service.ProjectStruct(id).FirstOrDefault();

                Console.WriteLine($"Project id: {result.Project.Id};");
                Console.WriteLine($"Project name: \"{result.Project.Name}\";");
                if (result.LongestTask != null)
                {
                    Console.WriteLine($"Longest task id: {result.LongestTask.Id};");
                    Console.WriteLine($"Longest task name: \"{result.LongestTask.Name}\";");
                }
                else
                    Console.WriteLine("Project has not tasks.");
                if (result.LongestTask != null)
                {
                    Console.WriteLine($"Shortest task id: {result.ShortestTask.Id};");
                    Console.WriteLine($"Shortest task name: \"{result.ShortestTask.Name}\";");
                }
                else
                    Console.WriteLine("Project has not tasks.");
                Console.WriteLine($"Users count: \"{result.UsersCount}\".");
                Console.WriteLine("--------------------------------------");
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id, try again.");
            }
            ListOfMenus[2].Show(false);
        }

        private static void GetMessages()
        {
            Console.Clear();
            var result = service.GetMessages();
            if (result == null)
                Console.WriteLine("Something wrong with file reading.");
            else if (result.Count == 0)
                Console.WriteLine("There are no messages in log file.");
            else
            {
                foreach (var item in result)
                {
                    Console.WriteLine($"{item.Create_At}: {item.Text}");
                }
            }

            ListOfMenus[2].Show(false);
        }
    }
}
